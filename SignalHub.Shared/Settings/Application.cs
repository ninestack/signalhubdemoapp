﻿namespace SignalHub.Shared.Settings
{
    public class Application
    {
        public string SignalHubAppId { get; set; }
        public string SignalHubApiSecret { get; set; }
        public string SignalHubUri { get; set; }
        public string SignalHubAPIUri { get; set; }
        public string SignalHubDashboardAppllicationBaseUri { get; set; }
        public string SignalHubDashboardAppllicationBaseUriStaging { get; set; }
    }

    public class AuthSettings
    {
        public string AuthType { get; set; }
    }
}
