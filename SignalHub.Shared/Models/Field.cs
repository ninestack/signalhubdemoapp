﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public abstract class FieldData
    {
        public enum FieldTypeDefinition { Unknown, Measure, Dimension, Id }
        public enum FieldDataTypeDefinition { NotSet, Number, String, DateTime, Latitude, Longitude, Polygon, Boolean }

        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SoureId { get; set; }
        public FieldTypeDefinition Type { get; set; }
        public FieldDataTypeDefinition DataType { get; set; }
        public string Kind { get; set; }
        public string Parent { get; set; }
        public string Group { get; set; }
    }

    public class Field : FieldData
    {
        public bool TypeChangedByUser { get; set; }
    }
    public class DrillField : FieldData
    {
        public string ResultName { get; set; }
    }
    public class FieldMeasure : DrillField // FieldData
    {
        public enum MeasureTypeDefinition { count, avg, median, min, max, sum, function }
        public MeasureTypeDefinition MeasureType { get; set; }
        //public string ResultName { get; set; }
        public bool AddedAutomatically { get; set; }
        public static FieldMeasure CreateFrom(Field field, MeasureTypeDefinition measureType)
            => new FieldMeasure { MeasureType = measureType, Name = field.Name, Type = field.Type, DataType = field.DataType, SoureId = field.SoureId };
        public static FieldMeasure CreateDefault()
            => new FieldMeasure { MeasureType = MeasureTypeDefinition.count, Title = "Count", Type = FieldTypeDefinition.Measure, DataType = FieldDataTypeDefinition.NotSet };
    }

    public class FieldDimension : DrillField // FieldData
    {
        public enum BinDefinition { NotSet, Minute, Hour, Day, Week, Month, Year }
        //public string ResultName { get; set; }
        public BinDefinition? Bin { get; set; }
        public double? BinRoundTo { get; set; }
        public static FieldDimension CreateFrom(Field field) => new FieldDimension { Name = field.Name, Type = field.Type, DataType = field.DataType, SoureId = field.SoureId };
        public static FieldDimension CreateFrom(FieldMeasure field) => new FieldDimension { Name = field.Name, Type = field.Type, DataType = field.DataType, SoureId = field.SoureId };
    }

    public class FieldOrder : DrillField // FieldData
    {
        public enum OrderDefinition { NotSet, Asc, Desc }
        public OrderDefinition Order { get; set; } = OrderDefinition.NotSet;
        //public string ResultName { get; set; }

        public static FieldOrder CreateFrom(object field, OrderDefinition order)
        {
            var measureField = field as Shared.Models.FieldMeasure;
            if (measureField != null)
                return CreateFrom(measureField, order);
            var dimensionField = field as Shared.Models.FieldDimension;
            if (dimensionField != null)
                return CreateFrom(dimensionField, order);

            return null;
        }

        public static FieldOrder CreateFrom(FieldMeasure field, OrderDefinition order)
             => new FieldOrder { Name = field.Name, ResultName = field.ResultName, SoureId = field.SoureId, Type = FieldTypeDefinition.Measure, DataType = field.DataType, Order = order };

        public static FieldOrder CreateFrom(FieldDimension field, OrderDefinition order)
            => new FieldOrder { Name = field.Name, ResultName = field.ResultName, SoureId = field.SoureId, Type = FieldTypeDefinition.Dimension, DataType = field.DataType, Order = order };
    }

    public class FieldFilter : FieldData
    {
        public enum ConditionDefinition { And, Or }
        public enum OperationDefinition { Equals, NotEquals, Between, AnyOf, NotAnyOf, Null, NotNull, Contains, StartsWith, EndsWith, True, False, Ago }

        public string Id { get; set; }
        public ConditionDefinition Condition { get; set; }
        public OperationDefinition Operation { get; set; }
        public IEnumerable<FieldFilter> Filters { get; set; }
        public object Value { get; set; }
        public IEnumerable<object> Values { get; set; }
        public Range Range { get; set; }
        public GeoBoundingBox GeoBoundingBox { get; set; }

        public static FieldFilter CreateFrom(Field field) => new FieldFilter { Id = Guid.NewGuid().ToString(), Name = field.Name, SoureId = field.SoureId, Type = field.Type, DataType = field.DataType };

        public bool IsSingleValueOperation() => !(IsRangeOperation() || IsAnyOfOperation() || IsNullOperation());
            //Operation == FieldFilter.OperationDefinition.Equals || Operation == FieldFilter.OperationDefinition.NotEquals || Operation == OperationDefinition.StartsWith || Operation == OperationDefinition.EndsWith;

        public bool IsRangeOperation() => 
            Operation == FieldFilter.OperationDefinition.Between || Operation == OperationDefinition.Ago;

        public bool IsAnyOfOperation() => 
            Operation == FieldFilter.OperationDefinition.AnyOf || Operation == FieldFilter.OperationDefinition.NotAnyOf;

        public bool IsNullOperation() =>
            Operation == OperationDefinition.Null || Operation == OperationDefinition.NotNull;
    }

    public class FieldMetadata //: FieldData
    {
        public string Language { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DescriptionInternal { get; set; }
        public long? Length { get; set; }
        public string Unit { get; set; }
        public bool GDPRSensitive { get; set; }
        public bool OfSpecialInterest { get; set; }
    }


    public enum TimeSpanTypeEnum { Second, Minute, Hour, Day, Week, Month, Year }

    public class Range
    {
        public object GreaterThanOrEqual { get; set; }
        public object LessThanOrEqualTo { get; set; }
    }

    public class GeoBoundingBox
    {
        public TopLeftCorner TopLeftCorner { get; set; }
        public BottomRightCorner BottomRightCorner { get; set; }
    }

    public class TopLeftCorner
    {
        public Location Location { get; set; }
    }

    public class BottomRightCorner
    {
        public Location Location { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }
}
