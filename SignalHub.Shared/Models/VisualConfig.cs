﻿using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class QueryConfig
    {
        public IEnumerable<FieldMeasure> MeasureFields { get; set; } = new List<FieldMeasure>();
        public IEnumerable<FieldDimension> DimensionFields { get; set; } = new List<FieldDimension>();
        public IEnumerable<FieldOrder> OrderFields { get; set; } = new List<FieldOrder>();
        public IEnumerable<FieldFilter> Filters { get; set; } = new List<FieldFilter>();
        public IEnumerable<Variable> Variables { get; set; } = new List<Variable>();
    }
}
