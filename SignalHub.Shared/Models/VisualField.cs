﻿namespace SignalHub.Shared.Models
{
    public class FieldDrilldown
    {
        public enum OrderDefinition { NotSet, Asc, Desc }
        public enum FieldTypeDefinition { NotSet, Measure, Dimension }

        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string EngineType { get; set; }
        public string AggregateType { get; set; }
        public FieldFilter Filter { get; set; }
        public FieldTypeDefinition FieldType { get; set; } = FieldTypeDefinition.NotSet;
        public OrderDefinition Order { get; set; } = OrderDefinition.NotSet;
    }
}
