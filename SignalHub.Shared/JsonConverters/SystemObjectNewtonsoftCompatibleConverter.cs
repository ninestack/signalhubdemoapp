﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SignalHub.Shared.JsonConverters
{
    /// <summary>
    /// Got this converter from here:
    ///  https://github.com/dotnet/corefx/blob/master/src/System.Text.Json/tests/Serialization/CustomConverterTests.Object.cs
    ///  More examples are included!
    ///  More info: https://weblogs.thinktecture.com/pawel/2019/10/aspnet-core-3-0-custom-jsonconverter-for-the-new-system_text_json.html
    /// </summary>

    public class SystemObjectNewtonsoftCompatibleConverter : JsonConverter<object>
    {
        public override object Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return ReadObject(ref reader);
        }

        private object ReadObject(ref Utf8JsonReader reader, IDictionary<string, object> obj = null)
        {
            if (reader.TokenType == JsonTokenType.Null)
            {
                return null;
            }
            if (reader.TokenType == JsonTokenType.True)
            {
                return true;
            }

            if (reader.TokenType == JsonTokenType.False)
            {
                return false;
            }

            if (reader.TokenType == JsonTokenType.Number)
            {
                if (reader.TryGetInt64(out long l))
                {
                    return l;
                }

                return reader.GetDouble();
            }

            if (reader.TokenType == JsonTokenType.String)
            {
                if (reader.TryGetDateTime(out DateTime datetime))
                {
                    return datetime;
                }

                return reader.GetString();
            }

            // Use JsonElement as fallback. Newtonsoft uses JArray or JObject.
            if (reader.TokenType == JsonTokenType.StartArray)
            {
                var arr = new List<dynamic>();
                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.EndArray)
                        return arr;
                    var newObj = this.ReadObject(ref reader);
                    arr.Add(newObj);
                }
                return arr;
            }
            if (reader.TokenType == JsonTokenType.StartObject)
            {
                var currentObj = obj == null ? new Dictionary<string, object>() : obj;
                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.PropertyName)
                    {
                        var propertyName = reader.GetString();
                        if (reader.Read())
                        {
                            var newObj = this.ReadObject(ref reader, currentObj);
                            currentObj.Add(propertyName, newObj);
                        }
                    }
                    else if (reader.TokenType == JsonTokenType.EndObject)
                    {
                        if (currentObj.ContainsKey("$type"))
                        {
                            var typeName = currentObj["$type"] as string;
                            return ToObject(typeName, currentObj);
                        }
                        return currentObj;
                    }
                    else
                    {
                        //this.ReadObject(ref reader, (object)currentObj);
                        using (JsonDocument document = JsonDocument.ParseValue(ref reader))
                        {
                            switch (document.RootElement.ValueKind)
                            {
                                case JsonValueKind.Object:
                                    var newObj = this.ReadObject(ref reader);
                                    if (obj == null)
                                        return newObj;
                                    //document.RootElement.na
                                    return obj;
                                case JsonValueKind.Array:
                                    var items = this.ReadObject(ref reader);
                                    return new List<dynamic> { items };
                                case JsonValueKind.Null:
                                case JsonValueKind.Undefined:
                                default:
                                    return null;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
        {
            throw new InvalidOperationException("Should not get here.");
        }

        public static object ToObject(string typeName, IDictionary<string, object> source) //where T : class, new()
        {
            //var someObject = new T();
            var objectType = Type.GetType(typeName);
            var createdObject = Activator.CreateInstance(objectType);
            //var someObjectType = someObject.GetType();
            var lowercaseDictionary = ConvertToLowercaseKeys(source);
            foreach (var propertyInfo in objectType.GetProperties())
            {
                var propertyName = propertyInfo.Name.ToLowerInvariant();
                if (lowercaseDictionary.ContainsKey(propertyName))
                {
                    if (propertyInfo.PropertyType.IsEnum)
                    {
                        var enumValue = Enum.Parse(propertyInfo.PropertyType, lowercaseDictionary[propertyName].ToString());
                        propertyInfo.SetValue(createdObject, enumValue, null);
                    }
                    else
                        propertyInfo.SetValue(createdObject, lowercaseDictionary[propertyName], null);
                }
            }
            return createdObject;
        }

        //public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        //{
        //    return source.GetType().GetProperties(bindingAttr).ToDictionary
        //    (
        //        propInfo => propInfo.Name,
        //        propInfo => propInfo.GetValue(source, null)
        //    );
        //}

        public static IDictionary<string, object> ConvertToLowercaseKeys(IDictionary<string, object> dictionary)
        {
            var lowercaseDictionary = new Dictionary<string, object>();
            foreach (var key in dictionary.Keys)
                lowercaseDictionary.Add(key.ToLowerInvariant(), dictionary[key]);

            return lowercaseDictionary;
        }
    }



    public class DynamicNewtonsoftCompatibleConverter : JsonConverter<dynamic>
    {
        public override dynamic Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return ReadObject(ref reader);
        }

        private dynamic ReadObject(ref Utf8JsonReader reader, IDictionary<string, object> obj = null)
        {
            if (reader.TokenType == JsonTokenType.True)
            {
                return true;
            }

            if (reader.TokenType == JsonTokenType.False)
            {
                return false;
            }

            if (reader.TokenType == JsonTokenType.Number)
            {
                if (reader.TryGetInt64(out long l))
                {
                    return l;
                }

                return reader.GetDouble();
            }

            if (reader.TokenType == JsonTokenType.String)
            {
                if (reader.TryGetDateTime(out DateTime datetime))
                {
                    return datetime;
                }

                return reader.GetString();
            }

            // Use JsonElement as fallback. Newtonsoft uses JArray or JObject.
            if (reader.TokenType == JsonTokenType.StartArray)
            {
                var arr = new List<dynamic>();
                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.EndArray)
                        return arr;
                    var newObj = this.ReadObject(ref reader);
                    arr.Add(newObj);
                }
                return arr;
            }
            if (reader.TokenType == JsonTokenType.StartObject)
            {
                var currentObj = obj == null ? new Dictionary<string, object>() : obj;
                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.PropertyName)
                    {
                        var propertyName = reader.GetString();
                        if (reader.Read())
                        {
                            var newObj = this.ReadObject(ref reader, currentObj);
                            currentObj.Add(propertyName, newObj);
                        }
                    }
                    else if (reader.TokenType == JsonTokenType.EndObject)
                        return ToDynamic(currentObj);
                    else
                    {
                        //this.ReadObject(ref reader, (object)currentObj);
                        using (JsonDocument document = JsonDocument.ParseValue(ref reader))
                        {
                            switch (document.RootElement.ValueKind)
                            {
                                case JsonValueKind.Object:
                                    var newObj = this.ReadObject(ref reader);
                                    if (obj == null)
                                        return newObj;
                                    //document.RootElement.na
                                    return obj;
                                case JsonValueKind.Array:
                                    var items = this.ReadObject(ref reader);
                                    return new List<dynamic> { items };
                                case JsonValueKind.Null:
                                case JsonValueKind.Undefined:
                                default:
                                    return null;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public override void Write(Utf8JsonWriter writer, dynamic value, JsonSerializerOptions options)
        {
            throw new InvalidOperationException("Should not get here.");
        }

        public static dynamic ToDynamic(IDictionary<string, object> obj)
        {
            var newObject = new System.Dynamic.ExpandoObject();
            var newObjectCollection = (ICollection<KeyValuePair<string, object>>)newObject;
            foreach (var keyValue in obj)
            {
                newObjectCollection.Add(keyValue);
            }
            return newObject;
        }
    }


    public class BaseClassObjectConverter<T> : JsonConverter<T>
    {
        public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            using (var doc = JsonDocument.ParseValue(ref reader))
            {
                if (doc.RootElement.TryGetProperty("$type", out var jsonElement))
                {
                    var typeName = jsonElement.GetString();
                    var objectType = Type.GetType(typeName);
                    if (objectType != null)
                    {
                        T obj = (T)JsonSerializer.Deserialize(doc.RootElement.GetRawText(), objectType, options);
                        return obj;
                    }
                }
                throw new Exception("Type is not allowed for conversion. Type is not found or empty.");
            }
        }

        public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();
            WriteTypeProperty(writer, value);
            foreach (var property in value.GetType().GetProperties())
            {
                var propertyName = options.PropertyNamingPolicy == null ? property.Name : options.PropertyNamingPolicy.ConvertName(property.Name);
                var propertyValue = property.GetValue(value);
                writer.WritePropertyName(propertyName);
                if (propertyValue == null)
                    writer.WriteNullValue();
                else
                    JsonSerializer.Serialize(writer, propertyValue, propertyValue.GetType(), options);
            }
            writer.WriteEndObject();
        }

        private void WriteTypeProperty(Utf8JsonWriter writer, T value)
        {
            var valueType = value.GetType();
            var typeName = $"{valueType.FullName}, {valueType.Assembly.GetName().Name}";
            writer.WritePropertyName("$type");
            writer.WriteStringValue(typeName);
        }
    }
}
