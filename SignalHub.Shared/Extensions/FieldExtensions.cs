﻿namespace SignalHub.Shared.Extensions
{
    public static class FieldExtensions
    {
        public static Models.FieldMetadata Translate(this Models.FieldData field, Services.SourceTranslator translator, string language = null)
        {
            return translator.Translate(field, language);
        }

        public static Models.FieldMetadata Translate(this Models.Field field, Services.SourceTranslator translator, string language = null)
        {
            return translator.Translate(field, language);
        }
    }
}
