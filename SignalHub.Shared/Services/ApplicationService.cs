﻿using SignalHub.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class ApplicationService
    {
        private HttpClient httpClient;
        private VisualService _visualService;

        public ApplicationService(HttpClient httpClient, VisualService visualService)
        {
            this.httpClient = httpClient;
            _visualService = visualService;
        }

        public async Task<Models.Application> Get(string applicationId)
        {
            var uri = $"application/{applicationId}";
            return await httpClient.GetJsonSignalHubAsync<Models.Application>(uri);
        }

        public async Task<IEnumerable<Models.Application>> All()
        {
            var uri = "application";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Application>>(uri);
        }

        public async Task<Models.Application> GetByUrl(string applicationUrl)
        {
            var uri = $"application/url/{applicationUrl}";
            return await httpClient.GetJsonSignalHubAsync<Models.Application>(uri);
        }

        public async Task<object> LinkVisual(string applicationId, string visualId)
        {
            return await LinkVisual(applicationId, new List<string> { visualId });
        }

        public async Task<object> LinkVisual(string applicationId, IEnumerable<string> visualIds)
        {
            var uri = $"application/{applicationId}/visual";
            return await httpClient.PutJsonSignalHubAsync<object>(uri, new { visualIds });
        }

        public async Task<object> UnlinkVisual(string applicationId, string visualId)
        {
            return await UnlinkVisual(applicationId, new List<string> { visualId });
        }
        public async Task<object> UnlinkVisual(string applicationId, IEnumerable<string> visualIds)
        {
            var uri = $"application/{applicationId}/visual";
            return await httpClient.DeleteJsonSignalHubAsync<object>(uri, new { visualIds });
        }

        public async Task<object> LinkDashboard(string applicationId, string dashboardId)
        {
            return await LinkDashboard(applicationId, new List<string> { dashboardId });
        }

        public async Task<object> LinkDashboard(string applicationId, IEnumerable<string> dashboardIds)
        {
            var uri = $"application/{applicationId}/dashboard";
            return await httpClient.PutJsonSignalHubAsync<object>(uri, new { dashboardIds });
        }

        public async Task<object> UnlinkDashboard(string applicationId, string dashboardId)
        {
            return await UnlinkDashboard(applicationId, new List<string> { dashboardId });
        }
        public async Task<object> UnlinkDashboard(string applicationId, IEnumerable<string> dashboardIds)
        {
            var uri = $"application/{applicationId}/dashboard";
            return await httpClient.DeleteJsonSignalHubAsync<object>(uri, new { dashboardIds });
        }

        public async Task<Models.Application> Upsert(Models.Application application)
        {
            var uri = "application";
            return await httpClient.PostJsonSignalHubAsync<Models.Application>(uri, application);
        }

        public async Task<object> UpdateDashboard(Models.Dashboard dashboard)
        {
            var uri = $"dashboard/applications";
            return await httpClient.PostJsonSignalHubAsync<object>(uri, dashboard);
        }
    }
}
