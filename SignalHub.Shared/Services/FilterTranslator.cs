﻿using SignalHub.Shared.Extensions;
using System;
using System.Linq;

namespace SignalHub.Shared.Services
{
    public class FilterTranslator
    {
        private SourceTranslator SourceTranslator { get; set; }

        public FilterTranslator(SourceTranslator sourceTranslator)
        {
            SourceTranslator = sourceTranslator;
        }

        public string Translate(Models.FieldFilter fieldFilter)
        {
            return $"{fieldFilter.Translate(SourceTranslator).Title} {PrintOperation(fieldFilter.Operation).ToLowerInvariant()} {PrintValue(fieldFilter)}";
        }

        public string PrintOperation(Models.FieldFilter.OperationDefinition operationDefinition)
        {
            return operationDefinition switch
            {
                Models.FieldFilter.OperationDefinition.AnyOf => "Any of",
                Models.FieldFilter.OperationDefinition.NotAnyOf => "Not any of",
                Models.FieldFilter.OperationDefinition.NotEquals => "Not equals",
                Models.FieldFilter.OperationDefinition.Null => "Null",
                Models.FieldFilter.OperationDefinition.NotNull => "Not null",
                Models.FieldFilter.OperationDefinition.StartsWith => "Starts with",
                Models.FieldFilter.OperationDefinition.EndsWith => "Ends with",
                Models.FieldFilter.OperationDefinition.Between => string.Empty,
                Models.FieldFilter.OperationDefinition.Ago => string.Empty,
                Models.FieldFilter.OperationDefinition.True => "Is true",
                Models.FieldFilter.OperationDefinition.False => "Is false",
                _ => operationDefinition.ToString().ToLowerInvariant()
            };
        }

        public string PrintValue(Models.FieldFilter fieldFilter)
        {
            if (fieldFilter.IsSingleValueOperation())
            {
                if (fieldFilter.Value != null)
                    return ConvertValue(fieldFilter.Value);
            }
            else if (fieldFilter.IsAnyOfOperation())
            {
                if (fieldFilter.Values != null && fieldFilter.Values.Any())
                    return string.Join(", ", fieldFilter.Values.Select(v => ConvertValue(v)));
            }
            else if (fieldFilter.IsRangeOperation())
            {
                if (fieldFilter.Range != null)
                {
                    if (fieldFilter.Operation == Models.FieldFilter.OperationDefinition.Ago)
                    {
                        if (fieldFilter.Range.GreaterThanOrEqual != null && fieldFilter.Range.LessThanOrEqualTo != null)
                            return $" between {ConvertValue(fieldFilter.Range.GreaterThanOrEqual)} ago and {ConvertValue(fieldFilter.Range.LessThanOrEqualTo)} ago";
                        if (fieldFilter.Range.GreaterThanOrEqual != null)
                            return $" >= {ConvertValue(fieldFilter.Range.GreaterThanOrEqual)} ago";
                        if (fieldFilter.Range.LessThanOrEqualTo != null)
                            return $" <= {ConvertValue(fieldFilter.Range.LessThanOrEqualTo)} ago";
                    }
                    else
                    {
                        if (fieldFilter.Range.GreaterThanOrEqual != null && fieldFilter.Range.LessThanOrEqualTo != null)
                            return $" between {ConvertValue(fieldFilter.Range.GreaterThanOrEqual)} and {ConvertValue(fieldFilter.Range.LessThanOrEqualTo)}";
                        if (fieldFilter.Range.GreaterThanOrEqual != null)
                            return $" >= {ConvertValue(fieldFilter.Range.GreaterThanOrEqual)}";
                        if (fieldFilter.Range.LessThanOrEqualTo != null)
                            return $" <= {ConvertValue(fieldFilter.Range.LessThanOrEqualTo)}";
                    }
                }
            }
            return string.Empty;
        }

        public string ConvertValue(object value)
        {
            return value switch
            {
                string v => v,
                DateTime v => ((DateTime)v).TimeOfDay == new TimeSpan(0) ? v.ToString("dd.MM.yyyy") : v.ToString("dd.MM.yyyy hh.mm.ss"),
                _ => value.ToString()
            };
        }

        public double? GetRangeValueNumberFrom(Shared.Models.FieldFilter filterField)
        {
            if (filterField.Range == null)
                return null;

            return TryParseDouble(filterField.Range.GreaterThanOrEqual);
        }

        public double? GetRangeValueNumberTo(Shared.Models.FieldFilter filterField)
        {
            if (filterField.Range == null)
                return null;

            return TryParseDouble(filterField.Range.LessThanOrEqualTo);
        }

        public double? TryParseDouble(object valueToConvert)
        {
            if (valueToConvert == null)
                return null;

            var culture = System.Globalization.CultureInfo.GetCultureInfo("nb-NO");
            var style = System.Globalization.NumberStyles.Number;
            var value = valueToConvert.ToString();
            if (double.TryParse(value, style, culture, out var valueLocal))
                return valueLocal;
            else if (double.TryParse(value, style, System.Globalization.CultureInfo.InvariantCulture, out var valueInvariant))
                return valueInvariant;
            else
                return null;
        }

    }
}
