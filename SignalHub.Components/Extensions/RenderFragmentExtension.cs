﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalHub.Components.Extensions
{
    public static class RenderFragmentExtension
    {
        //public static RenderFragment RenderFragment(this Shared.Models.Visual visual, SignalHub.Shared.Models.Source source)
        //public static RenderFragment RenderFragment(this Shared.Models.Visual visual, SignalHub.Shared.Loaders.Abstractions.Loader loader, bool isEditable = false)
        //{
        //    return new RenderFragment(builder =>
        //    {
        //        var type = Type.GetType($"SignalHub.Components.Visuals.{visual.Type}, SignalHub.Components");

        //        builder.OpenComponent(0, type);
        //        builder.AddAttribute(1, "Visual", visual);
        //        builder.AddAttribute(2, "Loader", loader);
        //        builder.AddAttribute(3, "Editable", isEditable);
        //        builder.SetKey(visual.Id);
        //        builder.CloseComponent();
        //    });
        //}

        //public static RenderFragment RenderFragment(this Shared.Models.Visual visual, SignalHub.Shared.Models.Source source, Action<object> referenceCaptureCallback)
        public static RenderFragment RenderFragment(this Shared.Models.Visual visual, SignalHub.Shared.Loaders.Abstractions.Loader loader, bool isEditable = false, Action<object> referenceCaptureCallback = null, Action<RenderTreeBuilder, int> withBuilderCallback = null)
        {
            return new RenderFragment(builder =>
            {
                var type = Type.GetType($"SignalHub.Components.Visuals.{visual.Type}, SignalHub.Components");
                var i = 0;
                builder.OpenComponent(i++, type);
                builder.AddAttribute(i++, "Visual", visual);
                builder.AddAttribute(i++, "Loader", loader);
                builder.AddAttribute(i++, "Editable", isEditable);
                if (referenceCaptureCallback != null)
                    builder.AddComponentReferenceCapture(i++, referenceCaptureCallback);
                if (withBuilderCallback != null)
                    withBuilderCallback(builder, i);
                builder.SetKey(visual.Id);
                builder.CloseComponent();
            });
        }

        public static RenderFragment RenderSettingsFragment(this Shared.Models.Visual visual, Action<object> referenceCaptureCallback = null)
        {
            return new RenderFragment(builder =>
            {
                var type = Type.GetType($"SignalHub.Components.Drilldowns.Settings.{visual.Type}Settings, SignalHub.Components");

                builder.OpenComponent(0, type);
                builder.AddAttribute(1, "Visual", visual);
                if (referenceCaptureCallback != null)
                    builder.AddComponentReferenceCapture(2, referenceCaptureCallback);
                builder.CloseComponent();
            });
        }

        public static RenderFragment RenderFieldAggregateFragment(this Shared.Models.Visual visual, Action<object> referenceCaptureCallback = null)
        {
            return new RenderFragment(builder =>
            {
                var type = Type.GetType($"SignalHub.Components.Drilldowns.FieldAggregates.{visual.Type}Aggregate, SignalHub.Components");

                builder.OpenComponent(0, type);
                builder.AddAttribute(1, "Visual", visual);
                if (referenceCaptureCallback != null)
                    builder.AddComponentReferenceCapture(2, referenceCaptureCallback);
                builder.CloseComponent();
            });
        }

        public static RenderFragment RenderFragment(this Shared.Models.FieldFilter filterField, Shared.Models.Source source, Shared.Models.Field field)//, Shared.Models.FieldMetadata fieldMetadata)
        {
            if (field == null)
                return new RenderFragment(builder => { });

            return new RenderFragment(builder =>
            {
                var type = Type.GetType($"SignalHub.Components.Drilldowns.Filters.{field.DataType}Filter");

                builder.OpenComponent(0, type);
                builder.AddAttribute(1, "Source", source);
                builder.AddAttribute(2, "FilterField", filterField);
                builder.AddAttribute(3, "Field", field);
                //builder.AddAttribute(4, "FieldMetadata", fieldMetadata);
                //builder.AddAttribute(4, "OnFilterChanged", OnFilterChanged);
                //builder.AddAttribute(5, "OnFilterRemoved", OnFilterRemoved);
                builder.CloseComponent();
            });
        }

        public static RenderFragment RenderFragment(this Shared.Models.Variable variable, Shared.Models.Source source)
        {
            if (variable == null)
                return new RenderFragment(builder => { });

            return new RenderFragment(builder =>
            {
                var type = Type.GetType($"SignalHub.Components.Drilldowns.Variables.{variable.Type}Variable");

                builder.OpenComponent(0, type);
                builder.AddAttribute(1, "Source", source);
                builder.AddAttribute(2, "Variable", variable);
                builder.CloseComponent();
            });
        }
    }
}
