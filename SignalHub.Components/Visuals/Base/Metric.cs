﻿using SignalHub.Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SignalHub.Shared.Extensions;
using System.Linq;

namespace SignalHub.Components.Visuals.Base
{
    public abstract class Metric : Component
    {
        protected override bool CanLoadData() { return true; }

        protected override async Task<object> LoadData(IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
        {
            //if (Visual.Config.MeasureFields.Count() == 0)
            //    Visual.Config.MeasureFields = new List<FieldMeasure> { new FieldMeasure { ResultName = "value0", MeasureType = FieldMeasure.MeasureTypeDefinition.count } };

            if (Visual.Config.MeasureFields.Any())
            {
                var payload = Shared.Services.Payloads.CountPayload.CreateFrom(Visual.Config, Visual.Appearance?.Rows ?? 1, additionalFilters: additionalFilters, searchText: searchText);
                var data = await Loader.Metric(payload);
                return PrepareData(data);
            }
            else 
                return 0.0;
        }

        protected virtual object PrepareData(object rawData)
        {
            //return DataNormalizerService.Normalize(rawData as IEnumerable<dynamic>);
            return rawData;
        }

        public override async Task AddDimension(FieldDimension field)
        {
            // Do nothing
        }

        public override async Task AddMeasure(FieldMeasure field)
        {
            Visual.Config.MeasureFields = new List<FieldMeasure> { field };
            await Reload();
        }

        public override async Task RemoveDimension(FieldDimension field)
        {
            // Do nothing
        }

        public override async Task RemoveMeasure(FieldMeasure field)
        {
            Visual.Config.MeasureFields = Visual.Config.MeasureFields.WithToList(l => l.Remove(field));
        }
    }
}
