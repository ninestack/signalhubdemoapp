﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SignalHub.Components.Extensions;
using SignalHub.Shared.Extensions;
using SignalHub.Shared.Models;

namespace SignalHub.Components.Visuals
{
    public partial class Table
    {
        protected const string defaultThemeName = "ag-theme-balham";

        private agGridJS agGridJS { get; set; }

        protected override async Task RenderJSComponents(bool firstRender)
        {
            var tableAppearance = Visual.Appearance as TableAppearance;

            if (firstRender)
            {
                var context = new
                {
                    id = Visual.Id,
                    onMovedColumnCaller = DotNetObjectReference.Create(this),
                    onMovedColumnFn = nameof(OnMovedColumn),

                    onResizedColumnCaller = DotNetObjectReference.Create(this),
                    onResizedColumnFn = nameof(OnResizedColumn),

                    onSelectedRowCaller = DotNetObjectReference.Create(this),
                    onSelectedRowFn = nameof(OnSelectedRow),

                    onClickedCellCaller = DotNetObjectReference.Create(this),
                    onClickedCellFn = nameof(OnClickedCell),

                    numDecimals = tableAppearance?.DefaultNumberOfDecimals,
                };
                agGridJS = DOMElement.agGridJS(JSRuntime);
                await agGridJS.Create(context);
            }
            else
            {
                var context = new { numDecimals = tableAppearance?.DefaultNumberOfDecimals };
                await agGridJS.UpdateContext(context);
            }
        }

        protected override async Task RenderData(object data)
        {
            var columns = new List<object>();
            var tableAppearance = Visual.Appearance as TableAppearance;
            if (Visual.Config.DimensionFields.Count() == 0)
            {
                var source = await GetVisualSource();
                var dimensionFields = CreateDimensions(source).OrderWith(tableAppearance.TableFields);
                foreach (var sourceField in dimensionFields)
                    columns.Add(CreateGridColumn(sourceField, sourceField.ResultName, tableAppearance));
            }
            else
            {
                var allFields = new List<DrillField>()
                    .AddMany(Visual.Config.DimensionFields, Visual.Config.MeasureFields)
                    .OrderWith(tableAppearance.TableFields);
                allFields.ToList().ForEach(f => columns.Add(CreateGridColumn(f, f.ResultName, tableAppearance)));

                //allFields.AddMany(Visual.Config.MeasureFields);
                //Visual.Config.DimensionFields.OrderWith(tableAppearance.TableFields).ToList().ForEach(f => columns.Add(CreateGridColumn(f, f.ResultName, tableAppearance)));
                //Visual.Config.MeasureFields.OrderWith(tableAppearance.TableFields).ToList().ForEach(f => columns.Add(CreateGridColumn(f, f.ResultName, tableAppearance)));
            }
            agGridJS = DOMElement.agGridJS(JSRuntime);
            await agGridJS.SetColumns(columns);
            await agGridJS.SetData(data);
        }

        protected object CreateGridColumn(FieldData field, string fieldResultName, TableAppearance tableAppearance)
        {
            var fieldAppearance = tableAppearance?.TableFields?.FirstOrDefault(f => f != null && f.Name == fieldResultName);
            return new
            {
                headerName = field.Translate(Translator).Title,
                field = fieldResultName,
                cellClass = GetCellClass(field),
                width = fieldAppearance?.CellWidth,
                resizable = Editable,
                suppressMovable = !Editable,
                hide = fieldAppearance?.Hidden,
                _datatype = field.DataType.ToString(),
            };
        }

        protected string GetCellClass(FieldData field)
        {
            switch (field.DataType)
            {
                case FieldData.FieldDataTypeDefinition.DateTime:
                case FieldData.FieldDataTypeDefinition.Latitude:
                case FieldData.FieldDataTypeDefinition.Longitude:
                case FieldData.FieldDataTypeDefinition.Number:
                    return "text-right";
                default:
                    return "text-left";
            }
        }

        protected override async Task OnBeforeLoad()
        {
            await agGridJS.ShowLoader();
            SetDimensionAndMeasureResultNames();
        }

        protected override async Task OnAfterLoad()
        {
            await agGridJS.HideLoader();
        }

        public override async Task SetSelectedRow(int index)
        {
            await agGridJS.SetSelectedRow(index);
            await base.SetSelectedRow(index);
        }

        public override async Task SetSelectedRows(IEnumerable<int> indices)
        {
            await agGridJS.SetSelectedRows(indices);
            await base.SetSelectedRows(indices);
        }

        public override string GetSelectedTheme()
        {
            var selectedThemeName = base.GetSelectedTheme();
            return string.IsNullOrWhiteSpace(selectedThemeName) ? defaultThemeName : selectedThemeName;
        }

        //protected void EnsureAllFields()
        //{
        //    var tableAppearance = Visual.Appearance as TableAppearance;
        //    var index = 0;
        //    var drillFields = new List<DrillField>().AddMany(Visual.Config.DimensionFields, Visual.Config.MeasureFields);
        //    foreach (var drillField in drillFields)
        //    {
        //        var field = tableAppearance.TableFields.FirstOrDefault(f => f.Name == drillField.ResultName);
        //        if (field == null)
        //        {
        //            field = new TableFieldAppearance { Name = drillField.ResultName };
        //            tableAppearance.TableFields.Add(field);
        //        }
        //        field.Index = index++;
        //    }
        //}

        //[JSInvokable]
        //public override async Task OnMovedColumn(string fieldResultName, int movedToIndex)
        //{
        //    EnsureAllFields();
        //    // order dimension and measure fields
        //    //Rearrange();
        //    // move
        //    // order appearance fields
        //    var tableAppearance = Visual.Appearance as TableAppearance;
        //    var fieldToMove = tableAppearance.TableFields.FirstOrDefault(f => f.Name == fieldResultName);
        //    tableAppearance.TableFields.Remove(fieldToMove);
        //    tableAppearance.TableFields.Insert(movedToIndex, fieldToMove);
        //    int i = 0;
        //    tableAppearance.TableFields.ToList().ForEach(f => f.Index = i++);
        //    tableAppearance.TableFields = tableAppearance.TableFields.OrderBy(f => f.Index).ToList();
        //}


        //[JSInvokable]
        //public override async Task OnResizedColumn(string fieldResultName, int width)
        //{
        //    var tableAppearance = Visual.Appearance as TableAppearance;
        //    var field = tableAppearance.TableFields.FirstOrDefault(f => f.Name == fieldResultName);
        //    if (field == null)
        //    {
        //        field = new TableFieldAppearance { Name = fieldResultName };
        //        tableAppearance.TableFields.Add(field);
        //    }
        //    field.CellWidth = width;
        //}

        //[JSInvokable]
        //public virtual async Task OnClickedCell(string fieldResultName, int rowIndex, System.Text.Json.JsonElement value)
        //{
        //}


        //protected void SetDimensionAndMeasureResultNames()
        //{
        //    for (var i = 0; i < Visual.Config.DimensionFields.Count(); i++)
        //    {
        //        var field = Visual.Config.DimensionFields.ElementAt(i);
        //        if (string.IsNullOrWhiteSpace(field.ResultName))
        //            field.ResultName = $"key{i}";
        //    }
        //    for (var i = 0; i < Visual.Config.MeasureFields.Count(); i++)
        //    {
        //        var field = Visual.Config.MeasureFields.ElementAt(i);
        //        if (string.IsNullOrWhiteSpace(field.ResultName))
        //            field.ResultName = $"value{i}";
        //    }
        //}
    }
}
