﻿using System.Collections.Generic;

namespace Kundkartan.Shared.Settings
{
    public class Application : SignalHub.Shared.Settings.Application
    {
        public KundkartanSettings Kundkartan { get; set; }
        public ValuguardSettings Valuguard { get; set; }
    }
    public class ValuguardSettings
    {
        public string Uri { get; set; }
        public string APIUri { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class KundkartanSettings
    {
        public DashboardSettings Dashboard { get; set; }
        public LeafletSettings Leaflet { get; set; }
    }

    public class DashboardSettings
    {
        public string FactsDashboardId { get; set; }
        public MigrationsDashboardSettings MigrationsDashboard { get; set; }
}

    public class MigrationsDashboardSettings
    {
        public string MoveFromTotalDashboardId { get; set; }
        public string MoveToTotalDashboardId { get; set; }
        public string MoveFromPercentDashboardId { get; set; }
        public string MoveToPercentDashboardId { get; set; }
    }

    public class LeafletSettings
    {
        public string Id { get; set; } = "kundkartan";
        public double Latitude { get; set; } = 62.862091;
        public double Longitude { get; set; } = 15.820536;
        public double Zoom { get; set; } = 5;
        public double ZoomSnap { get; set; } = 0;
        public double ZoomDelta { get; set; } = 0.25;
        public List<LeafletProviders> Providers { get; set; }
    }

    public class LeafletProviders
    {
        public string Id { get; set; }
        public LeafletProviderSettings LeafletProvider { get; set; }
    }

    public class LeafletProviderSettings
    {
        public string Token { get; set; }
        public string Tileurl { get; set; }
        public string Attribution { get; set; }
    }
}
